export class Departure {
    private _route: string;
    private _destination: string;
    private _departureText: string;

    constructor(rawDeparture: any){
        this._route = rawDeparture.Route;
        this._destination = rawDeparture.Description
        this._departureText = rawDeparture.DepartureText
    }


    /**
     * Getter route
     * @return {string}
     */
	public get route(): string {
		return this._route;
	}

    /**
     * Getter destination
     * @return {string}
     */
	public get destination(): string {
		return this._destination;
	}

    /**
     * Getter departureText
     * @return {string}
     */
	public get departureText(): string {
		return this._departureText;
	}

    /**
     * Setter route
     * @param {string} value
     */
	public set route(value: string) {
		this._route = value;
	}

    /**
     * Setter destination
     * @param {string} value
     */
	public set destination(value: string) {
		this._destination = value;
	}

    /**
     * Setter departureText
     * @param {string} value
     */
	public set departureText(value: string) {
		this._departureText = value;
	}

}