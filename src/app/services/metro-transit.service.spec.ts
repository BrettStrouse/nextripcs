import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { MetroTransitService } from './metro-transit.service';

describe('MetroTransitService', () => {
  let service: MetroTransitService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(MetroTransitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getRoutes should return value from observable',
    (done: DoneFn) => {
    service.getRoutes().subscribe(result => {
      expect(result[0].Route).toBe("901");
      done();
    });
  });

  it('#getDirections should return value from observable',
    (done: DoneFn) => {
    service.getDirections("2").subscribe(result => {
      expect(result[0].Text).toBe("Eastbound");
      done();
    });
  });

  it('#getStops should return value from observable',
    (done: DoneFn) => {
    service.getStops("2", "0").subscribe(value => {
      expect(value[0].Value).toBe("22HE");
      done();
    });
  });

  it('#getTimepointDepartures should return value from observable',
    (done: DoneFn) => {
    service.getTimepointDepartures("2", "0", "22HE").subscribe(value => {
      expect(value[0].Route).toBe("2");
      done();
    });
  });


});
