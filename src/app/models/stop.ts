export class Stop{
    private _text: string;
    private _value: string;

    constructor(rawStop: any){
        this._text = rawStop.Text
        this._value = rawStop.Value
    }


    /**
     * Getter text
     * @return {string}
     */
	public get text(): string {
		return this._text;
	}

    /**
     * Getter value
     * @return {string}
     */
	public get value(): string {
		return this._value;
	}

    /**
     * Setter text
     * @param {string} value
     */
	public set text(value: string) {
		this._text = value;
	}

    /**
     * Setter value
     * @param {string} value
     */
	public set value(value: string) {
		this._value = value;
	}

}