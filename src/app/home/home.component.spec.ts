import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Direction } from '../models/direction';
import { Route } from '../models/route';
import { Stop } from '../models/stop';
import { MetroTransitService } from '../services/metro-transit.service';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let metroServiceSpy: jasmine.SpyObj<MetroTransitService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#showMore() should toggle #showAll', () => {
    const comp = new HomeComponent(metroServiceSpy);
    expect(comp.showAll).toBe(false, 'off at first');
    comp.showMore();
    expect(comp.showAll).toBe(true, 'on after click');
    comp.showMore();
    expect(comp.showAll).toBe(false, 'off after second click');
  });

  it('#getDirections should get clear form', () => {
    let service = TestBed.inject(MetroTransitService);
    const comp = new HomeComponent(service);
    comp.route = new Route({Route: "2"})
    comp.getDirections()
    expect(comp.direction).toBeNull()
    expect(comp.stop).toBeNull()
    expect(comp.allDepartures).toEqual(new Array())
  });

  it('#getStops should get clear form', () => {
    let service = TestBed.inject(MetroTransitService);
    const comp = new HomeComponent(service);
    comp.route = new Route({Route: "2"})
    comp.direction = new Direction({Value: "0"})
    comp.getStops()
    expect(comp.stop).toBeNull()
    expect(comp.showAll).toBeFalse()
    expect(comp.allDepartures).toEqual(new Array())
  });

  it('#getTimepointDepartures should set departures button', () => {
    let service = TestBed.inject(MetroTransitService);
    const comp = new HomeComponent(service);
    comp.route = new Route({Route: "2"})
    comp.direction = new Direction({Value: "0"})
    comp.stop = new Stop({Value: "22HE"})
    comp.getTimepointDepartures()
    expect(comp.showAll).toBeFalse()
    expect(comp.icon).toBe('add_circle_outline')
  });
  
});
