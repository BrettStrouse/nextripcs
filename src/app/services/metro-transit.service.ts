import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MetroTransitService {

  constructor(
    private http: HttpClient
  ) { }

  public getRoutes(): Observable<any>{
    return this.http.get(API_URL + "/Routes")
  }

  public getDirections(routeId: string): Observable<any>{
    return this.http.get(API_URL + "/Directions/" + routeId)
  }

  public getStops(routeId: string, directionId: string): Observable<any>{
    return this.http.get(API_URL + "/Stops/" + routeId + "/" + directionId)
  }

  public getTimepointDepartures(routeId: string, directionId: string, stopId: string): Observable<any>{
    return this.http.get(API_URL + "/" + routeId + "/" + directionId + "/" + stopId)
  }
}
