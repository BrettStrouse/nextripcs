# NexTrip Target Case Study
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.5.

## Run the app
Install Node.js.
Run `npm install -g @angular/cli` in a terminal window.
Run `npm i` within the project folder.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Assumptions made
 - API would always be up and running (didn't add any UX error messages)
 - API would return values in the correct order
 
