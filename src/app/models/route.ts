export class Route {
    private _description: string;
    private _providerId: string;
    private _route: string;

    constructor(rawRoute: any){
        this._description = rawRoute.Description
        this._providerId = rawRoute.ProviderID
        this._route = rawRoute.Route
    }

    /**
     * Getter description
     * @return {string}
     */
	public get description(): string {
		return this._description;
	}

    /**
     * Getter providerId
     * @return {string}
     */
	public get providerId(): string {
		return this._providerId;
	}

    /**
     * Getter route
     * @return {string}
     */
	public get route(): string {
		return this._route;
	}

    /**
     * Setter description
     * @param {string} value
     */
	public set description(value: string) {
		this._description = value;
	}

    /**
     * Setter providerId
     * @param {string} value
     */
	public set providerId(value: string) {
		this._providerId = value;
	}

    /**
     * Setter route
     * @param {string} value
     */
	public set route(value: string) {
		this._route = value;
	}

}