import { Component, OnInit } from '@angular/core';
import { Departure } from '../models/departure';
import { Direction } from '../models/direction';
import { Route } from '../models/route';
import { Stop } from '../models/stop';
import { MetroTransitService } from '../services/metro-transit.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private transitService: MetroTransitService
  ){
  }

  displayedColumns: string[] = ['route', 'destination', 'departs'];


  route!: Route;
  direction!: Direction | null;
  stop!: Stop | null;
  
  routes: Route[] = new Array();
  directions: Direction[] = new Array()
  stops: Stop[] = new Array()
  allDepartures: Departure[] = new Array()

  icon = 'add_circle_outline'
  showAll = false;

  get departures(): Departure[]{
    if(this.allDepartures != null){
      if(this.allDepartures.length > 3 && !this.showAll){
        return this.allDepartures.slice(0, 3)
      }
    }
    return this.allDepartures
  }

  showMore(){
    this.showAll = !this.showAll
    if(this.showAll){
      this.icon = 'remove_circle_outline'
    }
    else this.icon = 'add_circle_outline'
  }

  getDirections(){
    //clear the form when route is changed
    this.direction = null;
    this.stop = null;
    this.allDepartures = new Array();

    this.transitService.getDirections(this.route.route).subscribe(result =>{
      console.log(result)
      this.directions = new Array()
      for(const direction of result){
        let newDir = new Direction(direction)
        this.directions.push(newDir)
      }
    })

  }

  getStops(){
    //clear the form when direction is changed
    this.stop = null
    this.allDepartures = new Array()
    this.showAll = false

    this.transitService.getStops(this.route.route, this.direction!.value).subscribe(result => {
      console.log(result)
      this.stops = new Array()
      for(const stop of result){
        let newStop = new Stop(stop)
        this.stops.push(newStop)
      }
    }, error =>{
      console.log(error)
    })
  }

  getTimepointDepartures(){
    //everytime a new stop is selected, reset the +departures button
    this.showAll = false
    this.icon = 'add_circle_outline'
    
    this.transitService.getTimepointDepartures(this.route.route, this.direction!.value, this.stop!.value).subscribe(result =>{
      console.log(result)
      this.allDepartures = new Array()
      for(const departure of result){
        let newDeparture = new Departure(departure)
        this.allDepartures.push(newDeparture)
      }
      console.log(this.allDepartures)
    }, error =>{
      console.log(error)
    })
  }

  ngOnInit(){
    this.transitService.getRoutes().subscribe(result =>{
      console.log(result)
      this.routes = new Array()
      for(const route of result){
        let newRoute = new Route(route)
        this.routes.push(newRoute)
      }
      console.log(this.routes)
    }, error =>{
      console.log(error)
    })
  }

}
